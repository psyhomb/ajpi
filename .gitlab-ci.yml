### Global variables
variables:
  SERIAL: 2023091400
  GOVERSION: "1.22"

### List of stages
stages:
  - build
  - release
  - deploy

### Shared content
.shared:
  tags:
    - gitlab-org

### Build Go binary
.build_go_binary:
  image: golang:${GOVERSION}
  stage: build
  extends: .shared
  before_script:
    # Install `task` build tool
    - bash <(curl -L https://taskfile.dev/install.sh) -d -b /usr/local/bin
  script:
    # Environment variables
    - export REVISION="${CI_COMMIT_TAG}"
    - |
      if [[ ${CI_PIPELINE_SOURCE} == "schedule" ]]; then
        export REVISION="${CI_COMMIT_SHORT_SHA}"
      fi
    # Build Go binary and create an artifact
    - mkdir target
    - task build
    - tar czvf target/${CI_PROJECT_NAME}-${REVISION}-${GOOS}-${GOARCH}.tar.gz ${CI_PROJECT_NAME}
  artifacts:
    # The value of expire_in is an elapsed time in seconds, unless a unit is provided
    # https://docs.gitlab.com/ee/ci/yaml/README.html#artifactsexpire_in
    #expire_in: 1h
    paths:
      - target/*.tar.gz
  rules:
    - if: $CI_COMMIT_TAG || $CI_PIPELINE_SOURCE == "schedule"

linux_amd64_go_binary:
  extends: .build_go_binary
  variables:
    GOOS: "linux"
    GOARCH: "amd64"

linux_arm64_go_binary:
  extends: .build_go_binary
  variables:
    GOOS: "linux"
    GOARCH: "arm64"

### Build and release container image
.release_container_image:
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  stage: release
  extends: .shared
  script:
    # Extract binary from the artificat
    - tar xzvf target/${CI_PROJECT_NAME}-${CI_COMMIT_TAG}-${GOOS}-${GOARCH}.tar.gz
    # Build container image with Kaniko and push it to container registry
    - |
      cat > /kaniko/.docker/config.json <<EOF
      {
        "auths": {
          "${CI_REGISTRY}": {
            "username": "${CI_REGISTRY_USER}",
            "password": "${CI_REGISTRY_PASSWORD}"
          }
        }
      }
      EOF
    - |
      if echo ${CI_COMMIT_TAG} | egrep -q "^[0-9]*\.[0-9]*\.[0-9]*$" && [[ ${GOOS} == "linux" ]] && [[ ${GOARCH} == "amd64" ]]; then
        OPTIONS="--destination ${CI_REGISTRY_IMAGE}:latest"
      fi
    - |
      /kaniko/executor \
        --custom-platform ${GOOS}/${GOARCH} \
        --context ${CI_PROJECT_DIR} \
        --dockerfile ${CI_PROJECT_DIR}/Dockerfile \
        --destination ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}-${GOOS}-${GOARCH} \
        ${OPTIONS}
  rules:
    - if: $CI_COMMIT_TAG

linux_amd64_container_image:
  extends: .release_container_image
  needs: ["linux_amd64_go_binary"]
  variables:
    GOOS: "linux"
    GOARCH: "amd64"

linux_arm64_container_image:
  extends: .release_container_image
  needs: ["linux_arm64_go_binary"]
  variables:
    GOOS: "linux"
    GOARCH: "arm64"

###################################################################################################
### Build, release and deploy ajpi container image with GeoIP DBs included (scheduled pipeline) ###
###################################################################################################

### Download GeoIP DBs
geoip_dbs:
  image:
    name: maxmindinc/geoipupdate:latest
    entrypoint: [""]
  stage: build
  extends: .shared
  variables:
    GEOIPUPDATE_EDITION_IDS: "GeoLite2-City GeoLite2-ASN"
    GEOIPUPDATE_PRESERVE_FILE_TIMES: "1"
    GEOIPUPDATE_VERBOSE: "1"
  script:
    - entry.sh
    - mkdir target
    - tar czvf target/geoip2.tar.gz -C /usr/share/GeoIP .
  artifacts:
    # The value of expire_in is an elapsed time in seconds, unless a unit is provided
    # https://docs.gitlab.com/ee/ci/yaml/README.html#artifactsexpire_in
    #expire_in: 1h
    paths:
      - target/*.tar.gz
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"

### Build and release container image with GeoIP DBs included
.release_container_image_with_geoip_dbs:
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  stage: release
  extends: .shared
  script:
    # Extract binary and geoip dbs from artificats
    - tar xzvf target/${CI_PROJECT_NAME}-${CI_COMMIT_SHORT_SHA}-${GOOS}-${GOARCH}.tar.gz
    - tar xzvf target/geoip2.tar.gz
    # Modify Dockerfile on the fly
    - sed -i '/COPY/ a COPY *.mmdb geoip2/' ${CI_PROJECT_DIR}/Dockerfile
    # Build container image with Kaniko and push it to container registry
    - |
      cat > /kaniko/.docker/config.json <<EOF
      {
        "auths": {
          "${CI_REGISTRY}": {
            "username": "${CI_REGISTRY_USER}",
            "password": "${CI_REGISTRY_PASSWORD}"
          }
        }
      }
      EOF
    - |
      if [[ ${GOOS} == "linux" ]] && [[ ${GOARCH} == "amd64" ]]; then
        OPTIONS="--destination ${CI_REGISTRY_IMAGE}:latest-gip-db"
      fi
    - |
      /kaniko/executor \
        --custom-platform ${GOOS}/${GOARCH} \
        --context ${CI_PROJECT_DIR} \
        --dockerfile ${CI_PROJECT_DIR}/Dockerfile \
        --destination ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}-${CI_PIPELINE_ID}-gip-db-${GOOS}-${GOARCH} \
        ${OPTIONS}
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"

linux_amd64_container_image_with_geoip_dbs:
  extends: .release_container_image_with_geoip_dbs
  needs:
    - linux_amd64_go_binary
    - geoip_dbs
  variables:
    GOOS: "linux"
    GOARCH: "amd64"

linux_arm64_container_image_with_geoip_dbs:
  extends: .release_container_image_with_geoip_dbs
  needs:
    - linux_arm64_go_binary
    - geoip_dbs
  variables:
    GOOS: "linux"
    GOARCH: "arm64"

### Deploy container image to Scaleway Serverless Containers platform
# https://www.scaleway.com/en/developers/api/serverless-containers/#path-containers-update-an-existing-container
scw_serverless_containers:
  image: alpine:latest
  stage: deploy
  extends: .shared
  needs:
    - linux_amd64_container_image_with_geoip_dbs
  variables:
    GOOS: "linux"
    GOARCH: "amd64"
  script:
    - apk add --no-cache curl jq
    - |
      curl -sSL -X PATCH \
        -H "X-Auth-Token: ${SCW_SECRET_KEY}" \
        -H "Content-Type: application/json" \
        -d '{
              "registry_image": "'${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}-${CI_PIPELINE_ID}-gip-db-${GOOS}-${GOARCH}'"
            }' \
        "https://api.scaleway.com/containers/v1beta1/regions/${SCW_REGION}/containers/${SCW_CONTAINER_ID}" | jq -r '.registry_image'
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
