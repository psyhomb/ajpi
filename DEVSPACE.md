Development
-----------

For developing, in local or remote Kubernetes environments, we use [DevSpace](https://www.devspace.sh).

Set Kubernetes context:

```plain
devspace use context
```

Set Kubernetes namespace:

```plain
devspace use namespace ajpi
```

Start dev environment (all files will be automatically synced between local dir and dev container):

```plain
devspace dev
```

Once development completed, destroy previously created environment with a single command:

```plain
devspace purge
```
