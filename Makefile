BIN_NAME = ajpi
REVISION ?= $(shell git rev-parse --short HEAD)
GOOS ?= linux
GOARCH ?= amd64
GOVERSION ?= 1.22
GOLDFLAGS ?= -s -w -X main.binName=${BIN_NAME} -X main.version=${REVISION}

.PHONY: build
build:
	go get -d
	CGO_ENABLED=0 go build -ldflags="${GOLDFLAGS}" -o ${BIN_NAME}

.PHONY: build_in_docker
build_in_docker:
	docker run --rm -e REVISION -e GOOS -e GOARCH -e GOLDFLAGS -v $(shell pwd):/usr/src/${BIN_NAME} -w /usr/src/${BIN_NAME} golang:${GOVERSION} make

.PHONY: build_docker
build_docker: build_in_docker
	docker build --force-rm -t ${BIN_NAME}:${REVISION}-${GOOS}-${GOARCH} . && rm -f ${BIN_NAME}
ifeq (${GOARCH},amd64)
	docker tag ${BIN_NAME}:${REVISION}-${GOOS}-${GOARCH} ${BIN_NAME}:latest
endif

.PHONY: update
update:
	go get -u

.PHONY: run
run:
	@go run -ldflags="${GOLDFLAGS}" .

.PHONY: help
help:
	@echo "build                - Compile go code and provide binary for ${GOOS}/${GOARCH} OS"
	@echo "build_in_docker      - Compile go code inside of docker container and provide binary for ${GOOS}/${GOARCH} OS"
	@echo "build_docker         - Compile go code and build docker image for ${GOOS}/${GOARCH} OS"
	@echo "update               - Update go packages and dependencies to their latest versions"
	@echo "run                  - Compile and run go code"
