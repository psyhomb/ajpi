ajpi
====

A simple service for looking up your IP address.

Build
-----

[Install `task`](https://taskfile.dev/installation) build tool.

Binary

```plain
task build
```

or by explicitly specifying OS and ARCH

```plain
GOOS=linux GOARCH=arm64 task build
```

Container image

```plain
task bc
```

or by explicitly specifying OS and ARCH

```plain
GOOS=linux GOARCH=arm64 task bc
```

Configuration
-------------

Supported environment variables:

| Env name                 |  Env (default) value      | Mandatory | Description                                          |
|:-------------------------|:--------------------------|:----------|:-----------------------------------------------------|
| `AJPI_IPADDRESS`         | 0.0.0.0                   | no        | Bind to IP address                                   |
| `AJPI_PORT`              | 8080                      | no        | Bind to port                                         |
| `AJPI_GEOIP_CITY_DB_FILE`| geoip2/GeoLite2-City.mmdb | no        | Path to GeoIP City DB                                |
| `AJPI_GEOIP_ASN_DB_FILE` | geoip2/GeoLite2-ASN.mmdb  | no        | Path to GeoIP ASN DB                                 |
| `AJPI_ENABLE_HEADERS`    | true                      | no        | Enable `/headers` route                              |
| `AJPI_ENABLE_ENVS`       | true                      | no        | Enable `/envs` route                                 |
| `AJPI_ENABLE_METRICS`    | true                      | no        | Enable Prometheus exporter (`/metrics` route)        |

Run
---

Run [container image](https://gitlab.com/psyhomb/ajpi/container_registry/1474602) without [GeoIP DBs](https://dev.maxmind.com/geoip/geoip2/geolite2/) (w/o additional data):

```bash
docker run -itd \
    --name ajpi \
    --restart always \
    -p 8080:8080 \
    -e AJPI_IPADDRESS="0.0.0.0" \
    -e AJPI_PORT="8080" \
    registry.gitlab.com/psyhomb/ajpi:latest
```

Run [container image](https://gitlab.com/psyhomb/ajpi/container_registry/1474602) with [GeoIP DBs](https://dev.maxmind.com/geoip/geoip2/geolite2/) (w/ additional data: country, city, asn_org):

```bash
docker run -itd \
    --name ajpi \
    --restart always \
    -p 8080:8080 \
    -e AJPI_IPADDRESS="0.0.0.0" \
    -e AJPI_PORT="8080" \
    -e AJPI_GEOIP_CITY_DB_FILE="geoip2/GeoLite2-City.mmdb" \
    -e AJPI_GEOIP_ASN_DB_FILE="geoip2/GeoLite2-ASN.mmdb" \
    registry.gitlab.com/psyhomb/ajpi:latest-gip-db
```

Usage
-----

**Note:** API will also parse `X-Real-IP` and `X-Forwarded-For` (has precedence) headers in order to work properly with reverse-proxies.

### Plain output

```bash
curl -sSL http://localhost:8080/
```

```bash
curl -sSL http://localhost:8080/ip
```

```bash
curl -sSL http://localhost:8080/hostname
```

```bash
curl -sSL http://localhost:8080/useragent
```

```bash
curl -sSL http://localhost:8080/country
```

```bash
curl -sSL http://localhost:8080/city
```

```bash
curl -sSL http://localhost:8080/timezone
```

```bash
curl -sSL http://localhost:8080/country-iso
```

```bash
curl -sSL http://localhost:8080/asn
```

```bash
curl -sSL http://localhost:8080/asnorg
```

### JSON output

Return all environment variables in response body.

**Note:** Environment variables could in some cases contain sensitive data, that's why it is possible to disable this route by setting `AJPI_ENABLE_ENVS` to `false` (default value is `true`).

```bash
curl -sSL http://localhost:8080/envs
```

Return all HTTP request headers in response body.

**Note:** Headers route could also be disabled by setting `AJPI_ENABLE_HEADERS` to `false` (default value is `true`).

```bash
curl -sSL http://localhost:8080/headers
```

Simulate responses with different HTTP status codes.

```bash
curl -sSL http://localhost:8080/status/404
```

Misc data:

- `clientIP`
- `Hostname`
- `userAgent`

Data collected from [GeoIP DBs](https://dev.maxmind.com/geoip/geoip2/geolite2/):

- `Country`
- `City`
- `PostalCode`
- `TimeZone`
- `CountryISOCode`
- `Asn`
- `AsnOrg`
- `Latitude`
- `Longitude`
- `CountryEU`

```bash
curl -sSL http://localhost:8080/all
```

If using [GeoIP DBs](https://dev.maxmind.com/geoip/geoip2/geolite2/) it is also possible to gather data about any IP being passed via `X-Forwarded-For` header or `ip=` query string parameter (if both are specified, query string parameter will have precedence).

```bash
curl -sSL -H "X-Forwarded-For: 1.1.1.1" http://localhost:8080/all
```

or

```bash
curl -sSL 'http://localhost:8080/all?ip=1.1.1.1'
```

### Prometheus

Return metrics in Prometheus compatible format.

**Note:** To disable this route we need to set `AJPI_ENABLE_METRICS` to `false` (default value is `true`).

```bash
curl -sSL http://localhost:8080/metrics
```

Nginx
-----

- [How to configure it and run it with Nginx proxy, Let's Encrypt and GeoIP2 automatic update?](./nginx)

Kubernetes
----------

- [How to run it on Kubernetes?](./orchestration/k8s)

ECS
---

- [How to run it on AWS ECS?](./orchestration/ecs)
