package main

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	geoip2 "github.com/oschwald/geoip2-golang"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/psyhomb/ajpi/validator"
)

// Program name and version.
var (
	binName string
	version string
)

// Environment variables.
var (
	ipAddress     string
	port          string
	cityDBFile    string
	asnDBFile     string
	ginMode       string
	enableHeaders bool
	enableEnvs    bool
	enableMetrics bool
)

// GeoIP2 databases.
var (
	cityDB         *geoip2.Reader
	asnDB          *geoip2.Reader
	isCityDBLoaded bool
	isAsnDBLoaded  bool
)

// Prometheus metrics.
var (
	httpRequestTotal = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "http_request_total",
			Help: "Total number of HTTP requests received",
		},
		[]string{"path", "code"},
	)
)

// Additional data (misc + geoip).
type additionalData struct {
	Country        string
	City           string
	PostalCode     string
	TimeZone       string
	CountryISOCode string
	Asn            string
	AsnOrg         string
	Latitude       float64
	Longitude      float64
	CountryEU      bool
}

func init() {
	log.Printf("[INFO] Running %v rev %v", binName, version)

	var err error

	ipAddress, _ = os.LookupEnv("AJPI_IPADDRESS")
	if len(ipAddress) == 0 {
		ipAddress = "0.0.0.0"
		log.Printf("[INFO] AJPI_IPADDRESS environment variable not present, falling back to default value %v", ipAddress)
	}

	port, _ = os.LookupEnv("AJPI_PORT")
	if len(port) == 0 {
		port = "8080"
		log.Printf("[INFO] AJPI_PORT environment variable not present, falling back to default value %v", port)
	}

	cityDBFile, _ = os.LookupEnv("AJPI_GEOIP_CITY_DB_FILE")
	if len(cityDBFile) == 0 {
		cityDBFile = "geoip2/GeoLite2-City.mmdb"
		log.Printf("[INFO] AJPI_GEOIP_CITY_DB_FILE environment variable not present, falling back to default value %v", cityDBFile)
	}

	asnDBFile, _ = os.LookupEnv("AJPI_GEOIP_ASN_DB_FILE")
	if len(asnDBFile) == 0 {
		asnDBFile = "geoip2/GeoLite2-ASN.mmdb"
		log.Printf("[INFO] AJPI_GEOIP_ASN_DB_FILE environment variable not present, falling back to default value %v", asnDBFile)
	}

	ginMode, _ = os.LookupEnv("GIN_MODE")
	if len(ginMode) == 0 {
		ginMode = "release"
	}

	eh, _ := os.LookupEnv("AJPI_ENABLE_HEADERS")
	enableHeaders, err = strconv.ParseBool(eh)
	if len(eh) == 0 || err != nil {
		enableHeaders = true
		log.Printf("[INFO] AJPI_ENABLE_HEADERS environment variable not present, falling back to default value %v", enableHeaders)
	}

	ee, _ := os.LookupEnv("AJPI_ENABLE_ENVS")
	enableEnvs, err = strconv.ParseBool(ee)
	if len(ee) == 0 || err != nil {
		enableEnvs = true
		log.Printf("[INFO] AJPI_ENABLE_ENVS environment variable not present, falling back to default value %v", enableEnvs)
	}

	em, _ := os.LookupEnv("AJPI_ENABLE_METRICS")
	enableMetrics, err = strconv.ParseBool(em)
	if len(em) == 0 || err != nil {
		enableMetrics = true
		log.Printf("[INFO] AJPI_ENABLE_METRICS environment variable not present, falling back to default value %v", enableMetrics)
	}

	cityDB, err = geoip2.Open(cityDBFile)
	if err == nil {
		isCityDBLoaded = true
	}

	asnDB, err = geoip2.Open(asnDBFile)
	if err == nil {
		isAsnDBLoaded = true
	}

	prometheus.MustRegister(httpRequestTotal)
}

func main() {
	gin.SetMode(ginMode)
	r := gin.Default()
	r.LoadHTMLFiles(binName + ".html.tmpl")

	// Prometheus - Middleware to increment the httpRequestTotal counter for each request.
	r.Use(func(c *gin.Context) {
		c.Next()
		path := c.Request.URL.Path
		code := strconv.Itoa(c.Writer.Status())
		httpRequestTotal.WithLabelValues(path, code).Inc()
	})
	if enableMetrics {
		r.GET("/metrics", gin.WrapH(promhttp.Handler()))
	}

	// Plain
	r.GET("/", renderHTML)
	r.GET("/ip", pongField)
	r.GET("/hostname", pongField)
	r.GET("/useragent", pongField)
	r.GET("/country", pongField)
	r.GET("/city", pongField)
	r.GET("/timezone", pongField)
	r.GET("/country-iso", pongField)
	r.GET("/asn", pongField)
	r.GET("/asnorg", pongField)
	// JSON
	r.GET("/all", pongData)
	r.GET("/status/:code", pongHTTPCode)
	if enableHeaders {
		r.GET("/headers", pongHeaders)
	}
	if enableEnvs {
		r.GET("/envs", pongEnvs)
	}

	tcpIPSocket := fmt.Sprintf("%v:%v", ipAddress, port)
	log.Printf("[INFO] Listening on %v", tcpIPSocket)
	r.Run(tcpIPSocket)
}

// renderHTML - Render HTML for Web clients and plain text for CLI clients.
func renderHTML(c *gin.Context) {
	if isCLIUserAgent(c.GetHeader("User-Agent")) {
		c.String(http.StatusOK, fmt.Sprintf("%v\n", getIP(c)))
		return
	}

	g := gin.H{
		"BinName":        binName,
		"ClientIP":       getIP(c),
		"Hostname":       getHostname(getIP(c)),
		"UserAgent":      c.GetHeader("User-Agent"),
		"IsCityDBLoaded": isCityDBLoaded,
		"IsAsnDBLoaded":  isAsnDBLoaded,
	}

	if isCityDBLoaded && isAsnDBLoaded {
		g["Additional"] = getAdditionalData(cityDB, asnDB, getIP(c))
	}

	c.HTML(http.StatusOK, binName+".html.tmpl", g)
}

// pongField - Return specific field value.
func pongField(c *gin.Context) {
	var (
		ad additionalData
		f  string
	)

	if isCityDBLoaded && isAsnDBLoaded {
		ad = getAdditionalData(cityDB, asnDB, getIP(c))
	}

	p := strings.Split(c.FullPath(), "/")[1]
	switch p {
	case "ip":
		f = getIP(c)
	case "hostname":
		f = getHostname(getIP(c))
	case "useragent":
		f = c.GetHeader("User-Agent")
	case "country":
		f = ad.Country
	case "city":
		f = ad.City
	case "timezone":
		f = ad.TimeZone
	case "country-iso":
		f = ad.CountryISOCode
	case "asn":
		f = ad.Asn
	case "asnorg":
		f = ad.AsnOrg
	}

	c.String(http.StatusOK, fmt.Sprintf("%v\n", f))
}

// pongHeaders - Return request headers in HTTP response.
func pongHeaders(c *gin.Context) {
	g := gin.H{
		"Host": c.Request.Host,
	}
	for k, v := range c.Request.Header {
		g[k] = strings.Join(v, " ")
	}

	c.PureJSON(http.StatusOK, g)
}

// pongEnvs - Return environment variables in HTTP response.
func pongEnvs(c *gin.Context) {
	g := gin.H{}
	for _, kv := range os.Environ() {
		l := strings.Split(kv, "=")
		g[l[0]] = l[1]
	}

	c.PureJSON(http.StatusOK, g)
}

// pongHTTPCode - Return received HTTP status code back in response.
func pongHTTPCode(c *gin.Context) {
	p := c.Param("code")
	code, err := strconv.Atoi(p)
	if err != nil || code < 100 || code > 599 {
		log.Printf("[ERROR] %v is not supported HTTP status code", p)
		code = http.StatusBadRequest
	}

	c.PureJSON(code, gin.H{
		"HttpCode": code,
	})
}

// pongData - Return all collected data in HTTP response.
func pongData(c *gin.Context) {
	g := gin.H{
		"ClientIP":  getIP(c),
		"Hostname":  getHostname(getIP(c)),
		"UserAgent": c.GetHeader("User-Agent"),
	}

	if isCityDBLoaded && isAsnDBLoaded {
		g["Additional"] = getAdditionalData(cityDB, asnDB, getIP(c))
	}

	c.PureJSON(http.StatusOK, g)
}

// getIP - If valid return IP address requested in query string parameter, otherwise return client's source IP.
func getIP(c *gin.Context) string {
	ip := c.Query("ip")
	if !validator.IsValid(validator.IPAddress(ip)) {
		ip = c.ClientIP()
	}

	return ip
}

// getHostname - Performs a reverse lookup for the given IP address, returning first hostname from the list.
func getHostname(ipAddress string) string {
	var h string
	l, _ := net.LookupAddr(ipAddress)
	if len(l) > 0 {
		h = l[0]
	}

	return h
}

// getAdditionalData - Collect GeoIP data.
func getAdditionalData(cityDB, asnDB *geoip2.Reader, ipAddress string) additionalData {
	// GeoIP DB data
	ip := net.ParseIP(ipAddress)
	city, _ := cityDB.City(ip)
	asn, _ := asnDB.ASN(ip)

	var asNum string
	if asn.AutonomousSystemNumber > 0 {
		asNum = fmt.Sprintf("AS%v", asn.AutonomousSystemNumber)
	}

	return additionalData{
		Country:        city.Country.Names["en"],
		City:           city.City.Names["en"],
		PostalCode:     city.Postal.Code,
		CountryISOCode: city.Country.IsoCode,
		CountryEU:      city.Country.IsInEuropeanUnion,
		TimeZone:       city.Location.TimeZone,
		Latitude:       city.Location.Latitude,
		Longitude:      city.Location.Longitude,
		Asn:            asNum,
		AsnOrg:         asn.AutonomousSystemOrganization,
	}
}

// isCLIUserAgent - Is this CLI client?
func isCLIUserAgent(ua string) bool {
	uaName := strings.Split(strings.ToLower(ua), "/")[0]

	switch uaName {
	case
		"curl",
		"wget",
		"httpie",
		"httpie-go",
		"go-http-client",
		"fetch libfetch":

		return true
	}
	return false
}
