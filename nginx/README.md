Nginx and LE
============

**Note:** Replace ajpi.example.com domain name in all files with your own domain name.

Create required directories:

```bash
mkdir -p /data/build/geoip2
mkdir -p /data/ajpi-nginx/{conf.d,www}
mkdir -p /data/ajpi-nginx/www/ajpi.example.com/.well-known/acme-challenge
```

Copy `docker-compose.yaml`, `ajpi.conf.disabled` and `ajpi-acme.conf` files to data directories:

```bash
cp docker-compose.yaml /data/build/
cp ajpi* /data/ajpi-nginx/conf.d/
```

Start Nginx server with `ajpi-acme.conf` configuration file first:

```bash
docker-compose -f /data/build/docker-compose.yaml up -d
```

Install `certbot` tool and generate Let's Encrypt certificate:

```bash
apt install certbot
certbot certonly --webroot -w /data/ajpi-nginx/www/ajpi.example.com -d ajpi.example.com
```

Disable Nginx ACME configuration file and then enable regular Nginx configuration file:

```bash
cd /data/ajpi-nginx/conf.d/
mv ajpi-acme.conf{,.disabled}
mv ajpi.conf{.disabled,}
```

Restart Nginx:

```bash
docker-compose -f /data/build/docker-compose.yaml restart ajpi-nginx
```

From this point onward certificate will be automatically renewed by `certbot` tool if expire in less than 30 days.

Check certificate status:

```bash
certbot certificates
```

Check certificate renewal status:

```bash
journalctl -fu certbot
```

[GeoIP Automatic updates](https://dev.maxmind.com/geoip/geoipupdate/):

**Note:** Replace `<ACCOUNT_ID>` and `<LICENSE_KEY>` with real values ([geoip-update.sh](./geoip-update.sh))

```bash
cp -a geoip-update.sh /usr/local/bin/
```

```bash
echo '0 0 * * 0  root  systemd-cat -t "geoip-update" /usr/local/bin/geoip-update.sh' > /etc/cron.d/geoip-update
```

Run this script manually only once to download GeoIP databases for the first time:

```bash
geoip-update.sh
```
