#!/bin/bash

export GEOIPUPDATE_ACCOUNT_ID="<ACCOUNT_ID>"
export GEOIPUPDATE_LICENSE_KEY="<LICENSE_KEY>"
export GEOIPUPDATE_EDITION_IDS="GeoLite2-City GeoLite2-ASN"
export GEOIPUPDATE_PRESERVE_FILE_TIMES="1"
export GEOIPUPDATE_VERBOSE="1"

docker run --rm \
    --name geoip-update \
    -e GEOIPUPDATE_ACCOUNT_ID \
    -e GEOIPUPDATE_LICENSE_KEY \
    -e GEOIPUPDATE_EDITION_IDS \
    -e GEOIPUPDATE_PRESERVE_FILE_TIMES \
    -e GEOIPUPDATE_VERBOSE \
    -v /data/build/geoip2:/usr/share/GeoIP \
    maxmindinc/geoipupdate

if [[ ${?} -eq 0 ]]; then
    docker-compose -f /data/build/docker-compose.yaml restart ajpi
fi
