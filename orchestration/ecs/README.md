ECS
===

Task
----

Replace placeholders in [ajpi-task.json](./ajpi-task.json) file with real values.

Placeholders list:

- `<aws_region>`

Register task from definition file (same function can be used for updates):

```bash
aws ecs register-task-definition --cli-input-json file://ajpi-task.json
```

List task definitions of specific family:

```bash
aws ecs list-task-definitions --family-prefix ajpi
```

Service
-------

Replace placeholders in [ajpi-svc.json](./ajpi-svc.json) file with real values.

Placeholders list:

- `<ecs_cluster_name>`
- `<target_group_arn>`
- `<iam_role_arn>`

Create service from definition file.

**Note:** ALB and target group has to be created and associated manually beforehand.

```bash
aws ecs create-service --cli-input-json file://ajpi-svc.json
```

Update service (set number of tasks):

```bash
aws ecs update-service --cluster ${ecs_cluster_name} --service ajpi --desired-count 2
```

Describe service:

```bash
aws ecs describe-services --cluster ${ecs_cluster_name} --services ajpi
```

List services:

```bash
aws ecs list-services --cluster ${ecs_cluster_name}
```
