Run ajpi on Kubernetes
======================

## Basic manifest

```plain
kubectl apply -f ajpi.yaml
```

## Blue-green and canary deployment strategies

We can also utilize `ajpi` to test progressive delivery.

Before we apply following manifests we first need to install and configure [Argo Rollouts](https://argo-rollouts.readthedocs.io/en/stable/installation) controller and [Prometheus](https://artifacthub.io/packages/helm/prometheus-community/prometheus) on Kubernetes.

Then we can apply blue-green and canary manifests to test progressive delivery.

### Blue-green

Apply blue-green manifest:

```plain
kubectl apply -f ajpi-argo-rollouts-blue-green.yaml
```

Watch deployment status:

```plain
kubectl argo rollouts get rollout ajpi -w
```

Deploy a new version of container image:

```plain
kubectl argo rollouts set image ajpi registry.gitlab.com/psyhomb/ajpi:latest-gip-db
```

Rollback will be automatically triggered if analysis fail but we can also initiate a manual rollback to previous running version if needed:

```plain
kubectl argo rollouts undo ajpi
```

After completing testing, we can delete all objects by running following command:

```
kubectl delete -f ajpi-argo-rollouts-blue-green.yaml
```

### Canary

Apply canary manifest:

```plain
kubectl apply -f ajpi-argo-rollouts-canary.yaml
```

Watch deployment status:

```plain
kubectl argo rollouts get rollout ajpi -w
```

Deploy a new version of container image:

```plain
kubectl argo rollouts set image ajpi registry.gitlab.com/psyhomb/ajpi:latest-gip-db
```

After completing testing, we can delete all objects by running following command:

```
kubectl delete -f ajpi-argo-rollouts-canary.yaml
```
