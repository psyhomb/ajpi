package validator

import (
	"regexp"
	"strconv"
	"strings"
)

// IPAddress - Internet Protocol Address.
type IPAddress string

// FQDN - Fully Qualified Domain Name.
type FQDN string

// SvcPort - Service Port.
type SvcPort int

type net interface {
	check() bool
}

// IsValid will check if any of supported types IPAddress, FQDN and SvcPort are valid.
func IsValid(n net) bool {
	return n.check()
}

func (ip IPAddress) check() bool {
	re := regexp.MustCompile(`^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$`)
	if !re.MatchString(string(ip)) {
		return false
	}

	ipOctets := strings.Split(string(ip), ".")
	for _, ipOctet := range ipOctets {
		if len(ipOctet) > 1 && string(ipOctet[0]) == "0" {
			return false
		}

		i, err := strconv.Atoi(ipOctet)
		if i > 255 || err != nil {
			return false
		}
	}

	return true
}

func (f FQDN) check() bool {
	re := regexp.MustCompile(`^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.){2,}([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9]){2,}$`)
	if !re.MatchString(string(f)) {
		return false
	}

	// Every individual part of fqdn must not be greater than 63 characters.
	fqdnParts := strings.Split(string(f), ".")
	for _, fqdnPart := range fqdnParts {
		if len(fqdnPart) > 63 {
			return false
		}
	}

	// Length of whole fqdn must not be greater than 255 charcters.
	if len(f) > 255 {
		return false
	}

	return true
}

func (p SvcPort) check() bool {
	re := regexp.MustCompile(`^[0-9]{1,5}$`)

	if !re.MatchString(strconv.Itoa(int(p))) || p > 65535 {
		return false
	}

	return true
}
