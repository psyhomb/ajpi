package validator

import (
	"testing"
)

func TestIPAddressIsValid(t *testing.T) {
	testValues := []map[string]string{
		{
			"valid": "1.1.1.1",
		},
		{
			"invalid": "10.255.255.256",
		},
		{
			"valid": "255.255.255.255",
		},
		{
			"invalid": "345.23.1.56",
		},
		{
			"valid": "192.168.0.1",
		},
		{
			"invalid": "a.b.c.d",
		},
		{
			"invalid": "192.168.1.999",
		},
		{
			"valid": "192.168.0.0",
		},
		{
			"valid": "0.0.0.0",
		},
		{
			"invalid": "01.02.1.1",
		},
	}

	for _, e := range testValues {
		for validity, value := range e {
			ip := IPAddress(value)

			if IsValid(ip) && validity == "invalid" {
				t.Errorf("Error: Function detected IP address %v as valid, yet this is an invalid IP address!", ip)
			}

			if !IsValid(ip) && validity == "valid" {
				t.Errorf("Error: Function detected IP address %v as invalid, yet this is a valid IP address!", ip)
			}
		}
	}
}

func TestFQDNIsValid(t *testing.T) {
	testValues := []map[string]string{
		{
			"valid": "foo.example.com",
		},
		{
			"invalid": "1.1.1.1",
		},
		{
			"valid": "foo.bar.example.com",
		},
		{
			"invalid": "-foo.1.example.com",
		},
		{
			"valid": "foo.1.example.com",
		},
		{
			"invalid": "foo..example.com",
		},
		{
			"invalid": "foooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo.example.com",
		},
		{
			"valid": "foo.bar.foo.bar.example.com",
		},
	}

	for _, e := range testValues {
		for validity, value := range e {
			f := FQDN(value)

			if IsValid(f) && validity == "invalid" {
				t.Errorf("Error: Function detected FQDN %v as valid, yet this is an invalid FQDN!", f)
			}

			if !IsValid(f) && validity == "valid" {
				t.Errorf("Error: Function detected FQDN %v as invalid, yet this is a valid FQDN!", f)
			}
		}
	}
}

func TestSvcPortIsValid(t *testing.T) {
	testValues := []map[string]int{
		{
			"valid": 443,
		},
		{
			"invalid": 65536,
		},
		{
			"valid": 80,
		},
		{
			"invalid": 876876876876,
		},
		{
			"valid": 8080,
		},
	}

	for _, e := range testValues {
		for validity, value := range e {
			p := SvcPort(value)

			if IsValid(p) && validity == "invalid" {
				t.Errorf("Error: Function detected port %v as valid, yet this is an invalid port!", p)
			}

			if !IsValid(p) && validity == "valid" {
				t.Errorf("Error: Function detected port %v as invalid, yet this is a valid port!", p)
			}
		}
	}
}
